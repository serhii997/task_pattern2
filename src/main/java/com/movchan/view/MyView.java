package com.movchan.view;

import com.movchan.controller.Controller;
import com.movchan.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static final Logger LOGGER = LogManager.getLogger(MyView.class);
    public static Scanner scanner = new Scanner(System.in);

    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methodMenu = new LinkedHashMap<>();
    private Controller controller = new ControllerImpl();

    public MyView(){
        menu.put("1", "created");
        menu.put("2", "sprint");
        menu.put("3", "to do");
        menu.put("4", "in progres");
        menu.put("5", "review");
        menu.put("6", "test");
        menu.put("7", "done");

        methodMenu.put("1", () -> controller.createdTask());
        methodMenu.put("2", () -> controller.sprint());
        methodMenu.put("3", () -> controller.toDo());
        methodMenu.put("4", () -> controller.inProgress());
        methodMenu.put("5", () -> controller.inProgress());
        methodMenu.put("6", () -> controller.codeReview());
        methodMenu.put("7", () -> controller.test());
        methodMenu.put("8", () -> controller.done());
        printMenu();
    }

    private void printMenu() {
        menu.forEach((key, value) -> System.out.println("`" + key + "` - " + "\t" + value));
    }

    public void run() {
        String command = "";
        do {
           // try {
                command = scanner.next().toLowerCase();
                methodMenu.get(command).print();
           //} /*catch (NullPointerException e) {
                LOGGER.error("wrongcommand");
                printMenu();
            //}*/

        } while (!command.equalsIgnoreCase("Q"));
    }

}
