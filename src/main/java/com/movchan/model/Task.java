package com.movchan.model;

import com.movchan.model.users.User;

import java.util.Objects;

public class Task {
    private String name;
    private String description;
    private User executor;
    private State state;

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Task(String name, String description, State state) {
        this.name = name;
        this.description = description;
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setExecutor(User executor) {
        this.executor = executor;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(name, task.name) &&
                Objects.equals(description, task.description) &&
                Objects.equals(executor, task.executor) &&
                state == task.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, executor, state);
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", executor=" + executor +
                ", state=" + state +
                '}';
    }
}
