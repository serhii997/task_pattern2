package com.movchan.model;

import java.util.List;

public class Sprint {
    private List<Task> tasks;

    public Sprint(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
