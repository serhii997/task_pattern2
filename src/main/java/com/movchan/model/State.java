package com.movchan.model;

public enum  State {
    CREATED, TO_DO, IN_PROGRESS, CODE_REVIEW, TEST, DONE
}
