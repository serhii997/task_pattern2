package com.movchan.model.users;

import com.movchan.model.Backlog;
import com.movchan.model.State;
import com.movchan.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProductOwner extends User {
    private static final Logger LOGGER = LogManager.getLogger(Developer.class);

    public ProductOwner(String name) {
        super(name);
    }

    public Backlog createTask(String name) {
        LOGGER.info(getName() + " Created Task");
        return new Backlog(name);
    }

    public void doneTask(Task task) {
        if (task.getState().equals(State.TEST)) {
            task.setExecutor(this);
            LOGGER.info(" - task moved to 'DONE' " + task.getState());
            task.setState(State.DONE);
        } else {
            LOGGER.info("bad state");
        }
    }

}
