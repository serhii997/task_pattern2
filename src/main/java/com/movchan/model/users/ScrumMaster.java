package com.movchan.model.users;

import com.movchan.model.Backlog;
import com.movchan.model.Sprint;
import com.movchan.model.State;
import com.movchan.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class ScrumMaster extends User {
    private static final Logger LOGGER = LogManager.getLogger(ScrumMaster.class);
    private Backlog backlog;

    public ScrumMaster(String name, Backlog backlog) {
        super(name);
        this.backlog = backlog;
    }

    public Backlog getBacklog() {
        return backlog;
    }

    public ScrumMaster(String name) {
        super(name);
    }

    public void fillProductBacklog(Backlog backlog) {
        LOGGER.info(getName() + " Created backlog " +backlog.getNameProduct());
        Task task1 = createTask("fix syntax error ", "you have not correct variables", State.CREATED);
        Task task2 = createTask("add role ", "each user must have ROLE", State.CREATED);
        Task task3 = createTask("fix db ", "you must use one сommunication between tables", State.CREATED);
        Task task4 = createTask("add pattern ", "you must have use pattern", State.CREATED);
        backlog.addTasks(Arrays.asList(task1, task2, task3, task4));
    }

    public void review(Task task){
        if (task.getState().equals(State.IN_PROGRESS)) {
            task.setExecutor(this);
            LOGGER.info(" - task moved to 'test' " + task.toString());
            task.setState(State.CODE_REVIEW);
        } else {
            LOGGER.info("bad state");
        }
    }


    public Sprint createSprint(List<Task> tasks) {
        return new Sprint(tasks);
    }

    private Task createTask(String name, String description, State state) {
        return new Task(name, description, state);
    }

}
