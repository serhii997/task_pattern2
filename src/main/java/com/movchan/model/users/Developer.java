package com.movchan.model.users;

import com.movchan.model.State;
import com.movchan.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Developer extends User {
    private static final Logger LOGGER = LogManager.getLogger(Developer.class);

    public Developer(String name) {
        super(name);
    }

    public void setTask(Task task) {
        if(task.getState().equals(State.CREATED)) {
            task.setExecutor(this);
            LOGGER.info(" - task moved 'to do' " + task.toString());
            task.setState(State.TO_DO);
        } else {
            LOGGER.info("bad state");
        }
    }

    public void doTask(Task task){
        if((task.getState().equals(State.TO_DO)) || (task.getState().equals(State.CODE_REVIEW))){
            task.setExecutor(this);
            LOGGER.info(" - task moved 'in progress' " + task.toString());
            task.setState(State.IN_PROGRESS);
        } else {
            LOGGER.info("bad state");
        }
    }

}
