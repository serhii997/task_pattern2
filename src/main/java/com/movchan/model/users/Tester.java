package com.movchan.model.users;

import com.movchan.model.State;
import com.movchan.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Tester extends User {
    private static final Logger LOGGER = LogManager.getLogger(Tester.class);

    public Tester(String name) {
        super(name);
    }

    public void testTask(Task task){
        if(task.getState().equals(State.CODE_REVIEW)){
            task.setExecutor(this);
            LOGGER.info(" - task moved to 'test' " + task.toString());
            task.setState(State.TEST);
        } else {
            LOGGER.info("bad state");
        }
    }
}
