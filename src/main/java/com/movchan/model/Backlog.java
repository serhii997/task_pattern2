package com.movchan.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Backlog {
    private static final Logger LOGGER = LogManager.getLogger(Backlog.class);

    private String nameProduct;
    private List<Task> tasks;

    public Backlog(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void addTasks(List<Task> tasks) {
        if(!tasks.isEmpty()) {
            this.tasks = tasks;
        } else {
            LOGGER.info("tasks is empty");
        }
    }
}
