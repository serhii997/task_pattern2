package com.movchan.controller;

public interface Controller {
    void createdTask();
    void sprint();
    void toDo();
    void inProgress();
    void codeReview();
    void test();
    void done();

}
