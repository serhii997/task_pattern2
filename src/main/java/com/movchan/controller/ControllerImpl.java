package com.movchan.controller;

import com.movchan.model.Backlog;
import com.movchan.model.Sprint;
import com.movchan.model.Task;
import com.movchan.model.users.Developer;
import com.movchan.model.users.ProductOwner;
import com.movchan.model.users.ScrumMaster;
import com.movchan.model.users.Tester;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ControllerImpl implements Controller {
    private static final Logger LOGGER = LogManager.getLogger(ControllerImpl.class);

    private Backlog backlog;
    private Sprint allTask;
    private ProductOwner owner;
    private ScrumMaster scrumMaster;

    @Override
    public void createdTask() {
        owner = new ProductOwner("Igor");
        this.backlog = owner.createTask("Tesla");
    }

    @Override
    public void sprint() {
        if(backlog != null) {
            scrumMaster = new ScrumMaster("Mykita");
            scrumMaster.fillProductBacklog(backlog);
            allTask = scrumMaster.createSprint(backlog.getTasks());
        }else {
            LOGGER.info("Firstly must have create Task");
        }
    }

    @Override
    public void toDo() {
        if (backlog != null) {
            for (Task task : allTask.getTasks()) {
                new Developer("Sergio").setTask(task);
            }
        } else {
            LOGGER.info("Firstly must have create Task");
        }
    }

    @Override
    public void inProgress() {
        if (backlog != null) {
            for (Task task : allTask.getTasks()) {
                new Developer("Oleg").doTask(task);
            }
        } else {
            LOGGER.info("Firstly must have create Task");
        }
    }

    @Override
    public void codeReview() {
        if (backlog != null) {
            for (Task task : allTask.getTasks()) {
                scrumMaster.review(task);
            }
        } else {
            LOGGER.info("Firstly must have create Task");
        }
    }

    @Override
    public void test() {
        if (backlog != null) {
            for (Task task : allTask.getTasks()) {
                new Tester("Orest").testTask(task);
            }
        } else {
            LOGGER.info("Firstly must have create Task");
        }
    }

    @Override
    public void done() {
        if (backlog != null) {
            for (Task task : allTask.getTasks()) {
                owner.doneTask(task);
            }
        } else {
            LOGGER.info("Firstly must have create Task");
        }
    }
}
